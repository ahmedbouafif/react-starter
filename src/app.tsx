import React, { useEffect, useState } from "react"

import ReactDOM from "react-dom"
import { BrowserRouter, Route, Routes } from "react-router-dom"

import UserContext from "@contexts/UserContext"

import Login from "./screens/Login"
import Things from "./screens/Things"

import "core-js/stable"
import "regenerator-runtime/runtime"
import "./index.css"

enum ApplicationScreens {
    LoginScreen,
    ThingsScreen,
}

/**
 * Global Layout for the application
 * @param onLogout when the user clicks logout button
 * @returns
 */
const Container = ({
    onLogout,
    children,
}: {
    onLogout: () => void
    children: React.ReactNode
}): React.ReactElement => {
    return (
        <div className="w-full h-full flex flex-col justify-center items-center">
            {children}

            <button
                className="block bg-blue-500 px-4 py-2 text-white hover:bg-blue-600 rounded-md mt-3"
                onClick={onLogout}>
                Logout
            </button>
        </div>
    )
}

/**
 * Uses SPA with current screen controlled via variable
 *
 * Wraps the authenticated (user exists) in a Container component
 * for the global style/layout of the app
 *
 * @see Container
 * @returns
 */
const SPA = (): React.ReactElement => {
    const [user, setUser] = useState<UserObject | undefined>(undefined)
    const [screen, setScreen] = useState<ApplicationScreens>(ApplicationScreens.LoginScreen)

    /* handle the user's login and set the correct screen */
    const onUserLoggedIn = (u: UserObject): void => {
        setUser(u)
        setScreen(ApplicationScreens.ThingsScreen)
    }

    const handleLogout = (): void => {
        setUser(undefined)
    }

    /* user is not logged in - show a login scree */
    if (!user || screen == ApplicationScreens.LoginScreen) return <Login onLogin={onUserLoggedIn} />

    return (
        <Container onLogout={handleLogout}>
            {screen == ApplicationScreens.ThingsScreen && <Things />}
        </Container>
    )
}

/**
 * Uses a React Router to switch patchs
 *
 * Wraps the authenticated (user exists) in a Container component
 * for the global style/layout of the app
 *
 * @see Container
 * @returns
 */
const Routed = (): React.ReactElement => {
    const [user, setUser] = useState<UserObject | undefined>(undefined)

    useEffect(() => {
        const stored = localStorage.getItem("user")
        if (stored && stored != "undefined") setUser(JSON.parse(stored))
    }, [])

    useEffect(() => {
        localStorage.setItem("user", JSON.stringify(user))
    }, [user])

    const handleLogout = (): void => {
        setUser(undefined)
    }

    if (!user) return <Login onLogin={setUser} />

    return (
        <UserContext.Provider value={user}>
            <BrowserRouter>
                <Container onLogout={handleLogout}>
                    <Routes>
                        <Route path="/" element={<Things />} />
                        <Route path="/things" element={<Things />} />
                    </Routes>
                </Container>
            </BrowserRouter>
        </UserContext.Provider>
    )
}

/**
 *
 * @param router -> Use react router or use SPA switching screens
 * @returns React.ReactElement
 */
const App = ({ router }: { router?: boolean }): React.ReactElement =>
    router ? <Routed /> : <SPA />

ReactDOM.render(<App router />, document.getElementById("root"))
