type UserObject = {
    id: number
    name: string
    token: string
}

/* Sample somethign object */
type Something = {
    id: number
    name: string
}
